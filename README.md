# README
EmojiRecordsは、音楽好きのためのシンプルなWEBアプリです。
あなたが好きなアルバムのジャケットと、その時の気分を絵文字💭で投稿しましょう。

##使用言語
Ruby 2.6.1
Ruby on Rails 5.2.3
