
$(document).on('turbolinks:load', function () {
  $(function () {
    $('#search_button').click(
      function () {
        var artistname = $('#new_micropost [name=artistname]').val();
        var albumname = $('#new_micropost [name=albumtitle]').val();
        var term = artistname + albumname;
        $.ajax({
          url: 'https://itunes.apple.com/search?',
          type: 'GET',
          data: {
            term: term,
            country: 'jp',
            media: 'music',
            entity: 'album',
            lang: 'ja_jp',
            limit: '10'
          },
          dataType: 'jsonp',
          timespan: 1000
        }).done(function (data) {
          var result = data.results[0];
          var resultcount = data.resultCount;
          if (result == null) {
            $("#image-box").children("img").attr({ 'src': '' });
            $('#searchresult').text("検索結果が見つかりません");
          }
          else {
            $("#searchresult").remove();
            $('#artistname').text(result.artistName);
            $('#albumtitle').text(result.collectionName);
            $('#artwork').text(result.artworkUrl100);
            var url = result.artworkUrl100;
            $("#image-box").children("img").attr({ 'src': url });
          }

        }).fail(function (jqXHR, textStatus, errorThrown) { //通信が失敗した時
          $("#span1").text(jqXHR.status); //例：404
          $("#span2").text(textStatus); //例：error
          $("#span3").text(errorThrown); //例：NOT FOUND
        });
      });
  })
});
