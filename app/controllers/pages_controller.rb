class PagesController < ApplicationController
  def index
    if user_signed_in?
    @user = current_user
    @micropost = current_user.microposts.build
    @feed_items = current_user.feed.paginate(page: params[:page])
    else
      flash[:alert] = "ログインしてください"
      redirect_to login_path
    end
  end

  def show
  end

  def search
  end

  def about
  end
end
