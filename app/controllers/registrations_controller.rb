class RegistrationsController < Devise::RegistrationsController

  def after_update_path_for(resource)
   sign_in_after_change_password? ? after_sign_in_path_for(resource) :
   index_path
  end

  def following
    @title = "フォロー"
    @user  = User.find(params[:id])
    @users = @user.following
    render 'show_follow'
  end

  def followers
    @title = "フォロワー"
    @user  = User.find(params[:id])
    @users = @user.followers
    render 'show_follow'
  end


  private

    # アップロードされた画像のサイズをバリデーションする
    def picture_size
      if picture.size > 5.megabytes
        set_flash_message! :notice, :"画像は5MB以下にしてくｄさい"
      end
    end
end
