class Itunes
  # プロパティの設定
  attr_accessor :artistname, :albumtitle, :artworkimg

  def initialize(artistname, albumtitle, artworkimg)
    @artistname = artistname
    @albumtitle = albumtitle
    @artworkimg = artworkimg
  end
end
