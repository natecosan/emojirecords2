class Micropost < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  #validates :content, presence: true
  #validates :artistname, presence: true
  #validates :albumtitle, presence:true
  validate :add_error_sample

  def add_error_sample
    # nameが空のときにエラーメッセージを追加する
    if albumtitle.blank?
      errors[:base] << "・アルバムタイトルが空欄です"
    end

    # 価格が空のときにエラーメッセージを追加する
    if content.blank?
      errors[:base] << "・投稿文が空欄です"
    end
  end
end
