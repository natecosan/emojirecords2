class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :lockable, :timeoutable, :omniauthable, omniauth_providers: [:twitter]
  has_many :microposts, dependent: :destroy
  mount_uploader :originalimage, ImageUploader
  validate :image_size
  acts_as_followable
  acts_as_follower

  def self.from_omniauth(auth)
    find_or_create_by(provider: auth["provider"], uid: auth["uid"]) do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.username = auth["info"]["nickname"]
      user.image = auth["info"]["image"]
    end
  end

  def self.new_with_session(params,session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"]) do |user|
        user.attributes = params
      end
    else
      super
    end
  end

  def feed
    Micropost.where("user_id IN (?) OR user_id = ?", all_following.map(&:id), id)
  end

  private
  def image_size
    if originalimage.size > 5.megabytes
      flash[:alert] = "画像は5MB以下にしてください。"
    end
  end
end
