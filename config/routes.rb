Rails.application.routes.draw do
  devise_for :users, controllers: { :omniauth_callbacks => "omniauth_callbacks"}
  resources :testsessions, only: :create
  resources :users, :only => [:index, :show]

   devise_scope :user do
     root :to => "devise/sessions#new"
     get 'login', to: 'devise/sessions#new'
     delete 'logout', to: 'devise/sessions#destroy'
     get '/index', to: 'pages#index'
   end
    root 'pages#index'
    get '/index', to: 'pages#index'
    get 'pages/show',to: 'pages#show', as: 'profile'
    get '/search', to: 'pages#search'
    get '/about', to:'pages#about'

    put 'users/follow/:user_id' => 'users#follow'
    put 'users/unfollow/:user_id' => 'users#unfollow'

    get 'users/follow_list/:user_id' => 'users#follow_list'
    get 'users/follower_list/:user_id' => 'users#follower_list'
   resources :microposts,          only: [:create, :destroy, :show]

   get '/search', to:"itunessearch#search"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
