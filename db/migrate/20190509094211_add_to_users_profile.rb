class AddToUsersProfile < ActiveRecord::Migration[4.2][5.2]
  def change
    add_column :users, :profile, :text
  end
end
