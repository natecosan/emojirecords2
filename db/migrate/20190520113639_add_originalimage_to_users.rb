class AddOriginalimageToUsers < ActiveRecord::Migration[4.2][5.2]
  def change
    add_column :users, :originalimage, :string
  end
end
